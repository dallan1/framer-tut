import { useControls } from "leva";
import "./App.css";
import ReactLogo from "./assets/react.svg";
import { Marquee } from "./Marquee";

function App() {
  const { content } = useControls({
    content: "Marquee",
  });

  return (
    <div className="App">
      <Marquee>
        <img src="/vite.svg" draggable={false} />
        {content}
        <img src={ReactLogo} draggable={false} />
      </Marquee>
    </div>
  );
}

export default App;
