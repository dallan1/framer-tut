import { wrap } from "@motionone/utils";
import {
  useMotionValue,
  useTransform,
  useAnimationFrame,
  motion,
} from "framer-motion";
import { useControls, folder } from "leva";
import {
  PropsWithChildren,
  useRef,
  RefObject,
  useState,
  useCallback,
  useMemo,
  useEffect,
} from "react";

import "./Marquee.css";

type MarqueeProps = PropsWithChildren<unknown>;

/**
 * Infinitely scrolling interactive marquee.
 *
 *           <-- window.innerWidth -->
 *           _________________________
 *          |                        |
 * |children| clone  | clone  | clone  |
 *          |________________________|
 * <--loop-->
 *
 * Algorithm:
 *   1. Calculate how many clones of the children are needed to fill the viewport, plus one. (Math.ceil(window.innerWidth / children.offsetWidth) + 1)
 *   2. Loop the x position of the marque between -children.offsetWidth - 0.
 */
export const Marquee = ({ children }: MarqueeProps) => {
  const { velocity, dragEnabled, dragPower, dragTimeConstant } =
    useMarqueeControlGUI();

  const ref = useRef<HTMLDivElement>(null);

  // 1. Calculate how many clones of the children are needed to fill the viewport, plus one.
  const { contentWidth, howManyCopiesWeNeed } = useCalculateCopiesNeeded(ref);

  const baseX = useMotionValue(0);
  // 2. Loop the x position of the marque between -marquee.offsetWidth - 0.
  const x = useTransform(baseX, (val) => wrap(-contentWidth.current, 0, val));

  const isPressed = useRef(false);

  const prevT = useRef(-1);

  useAnimationFrame((t) => {
    if (prevT.current === -1) prevT.current = t;

    const deltaT = t - prevT.current;

    const moveBy = velocity * (deltaT / 1000);

    if (!isPressed.current) {
      baseX.set(baseX.get() + moveBy);
    }

    prevT.current = t;
  });

  return (
    <motion.div
      className="marquee"
      drag={dragEnabled ? "x" : false}
      dragTransition={
        dragEnabled
          ? { power: dragPower, timeConstant: dragTimeConstant }
          : undefined
      }
      onMouseDown={dragEnabled ? () => (isPressed.current = true) : undefined}
      onMouseUp={dragEnabled ? () => (isPressed.current = false) : undefined}
      onMouseLeave={dragEnabled ? () => (isPressed.current = false) : undefined} // onMouseUp isn't triggered if the mouse is let go outside of the marquee
      _dragX={dragEnabled ? baseX : undefined}
      style={{ x }}
    >
      <span ref={ref}>{children}</span>
      <span>
        {Array.from({ length: howManyCopiesWeNeed - 1 }).map(() => children)}
      </span>
    </motion.div>
  );
};

/**
 * Calculates the number of times to clone the children based off the viewport width and width of the children.
 *   - Recalculates automatically when the size of the window or children changes.
 */
const useCalculateCopiesNeeded = (ref: RefObject<HTMLDivElement>) => {
  const contentWidth = useRef(0);
  const [howManyCopiesWeNeed, setHowManyCopiesWeNeed] = useState(0);
  const [, setGUI] = useControls(() => ({
    noOfCopies: {
      value: 0,
      disabled: true,
    },
  }));

  // Function that calculates the number of copies needed
  const update = useCallback(() => {
    contentWidth.current = ref.current!.offsetWidth;

    // Calculate how many clones of the children are needed to fill the viewport, plus one.
    // We can't divide by zero so fallback to 0 if the width of the children is 0.
    const noCopiesNeeded =
      ref.current!.offsetWidth === 0
        ? 0
        : Math.ceil(window.innerWidth / ref.current!.offsetWidth) + 1;

    setHowManyCopiesWeNeed(noCopiesNeeded);

    // Update the control panel GUI
    setGUI({
      noOfCopies: noCopiesNeeded,
    });
  }, []);

  // The resize observer that listens for changes to the size of the children.
  // Memoised to prevent memory leak/unnecessary creation.
  const resizeObserver = useMemo(
    () =>
      new ResizeObserver(() => {
        update();
      }),
    []
  );

  // Set up resize listeners for the window and children
  useEffect(() => {
    // Store a local reference of the current ref to the element that wraps the children incase the ref was changed between
    // mount and unmount to prevent memory leak as resizeObserver.unobserve needs a reference to the original element.
    const currentRef = ref.current;

    // Recalculate on window resize
    addEventListener("resize", update);

    // Recalculate on children resize
    resizeObserver.observe(currentRef!);

    // Calculate on mount
    update();

    // Clean up listeners on unmount
    return () => {
      removeEventListener("resize", update);
      resizeObserver.unobserve(currentRef!);
    };
  }, []);

  return { contentWidth, howManyCopiesWeNeed };
};

/**
 * Adds marquee controls to the control panel.
 */
const useMarqueeControlGUI = () => {
  const controls = useControls({
    velocity: {
      value: 70,
      min: -2000,
      max: 2000,
      step: 1,
    },
    noOfCopies: {
      value: 0,
      disabled: true,
    },
    "Drag settings": folder({
      dragEnabled: {
        value: true,
        label: "enabled",
      },
      dragPower: {
        label: "power",
        value: 0.1,
        min: 0,
        max: 2,
      },
      dragTimeConstant: {
        label: "time",
        value: 200,
        min: 0,
        max: 5000,
      },
    }),
  });

  return controls;
};
